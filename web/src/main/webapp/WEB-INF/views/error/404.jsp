<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2015/10/10
  Time: 10:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <meta charset="utf-8"/>
    <title>404-页面未找到</title>

    <meta name="description" content="404 Error Page"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- basic styles -->

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="${contextPath}/resources/css/font-awesome.min.css"/>

    <!--[if IE 7]>
    <link rel="stylesheet" href="${contextPath}/resources/css/font-awesome-ie7.min.css"/>
    <![endif]-->

    <!-- page specific plugin styles -->

    <!-- fonts -->

    <link rel="stylesheet" href="${contextPath}/resources/css/ace-fonts.css"/>

    <!-- ace styles -->

    <link rel="stylesheet" href="${contextPath}/resources/css/ace.min.css"/>
    <link rel="stylesheet" href="${contextPath}/resources/css/ace-rtl.min.css"/>
    <link rel="stylesheet" href="${contextPath}/resources/css/ace-skins.min.css"/>

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="${contextPath}/resources/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->

    <script src="${contextPath}/resources/js/ace-extra.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
    <script src="${contextPath}/resources/js/html5shiv.js"></script>
    <script src="${contextPath}/resources/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="main-container" id="main-container">
    <div class="main-container-inner">
        <div>
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->

                        <div class="error-container">
                            <div class="well">
                                <h1 class="grey lighter smaller">
											<span class="blue bigger-125">
												<i class="icon-sitemap"></i>
												404
											</span>
                                    页面未找到.
                                </h1>

                                <hr/>
                                <h3 class="lighter smaller">该页面未找到!</h3>

                                <div>
                                    <div class="space"></div>
                                    <h4 class="smaller">你可以尝试:</h4>

                                    <ul class="list-unstyled spaced inline bigger-110 margin-15">
                                        <li>
                                            <i class="icon-hand-right blue"></i>
                                            检查url是否正确.
                                        </li>
                                        <li>
                                            <i class="icon-hand-right blue"></i>
                                            联系我们.
                                        </li>
                                    </ul>
                                </div>

                                <hr/>
                                <div class="space"></div>

                                <div class="center">
                                    <a href="javascript:window.history.go(-1);" class="btn btn-grey">
                                        <i class="icon-arrow-left"></i>
                                        返回
                                    </a>

                                    <a href="/index.htm" class="btn btn-primary">
                                        <i class="icon-dashboard"></i>
                                        首页
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.page-content -->
        </div>
        <!-- /.main-content -->
    </div>
    <!-- /.main-container-inner -->


</div>
</body>
</html>
