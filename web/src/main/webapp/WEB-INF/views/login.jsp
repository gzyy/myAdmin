<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2015/4/18
  Time: 18:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>登录-Tanx</title>

    <meta name="description" content="User login page"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- basic styles -->

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="${contextPath}/resources/css/font-awesome.min.css"/>

    <!--[if IE 7]>
    <link rel="stylesheet" href="${contextPath}/resources/css/font-awesome-ie7.min.css"/>
    <![endif]-->

    <!-- page specific plugin styles -->

    <!-- fonts -->

    <link rel="stylesheet" href="${contextPath}/resources/css/ace-fonts.css"/>

    <!-- ace styles -->

    <link rel="stylesheet" href="${contextPath}/resources/css/ace.min.css"/>
    <link rel="stylesheet" href="${contextPath}/resources/css/ace-rtl.min.css"/>

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="${contextPath}/resources/css/ace-ie.min.css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="${contextPath}/resources/js/html5shiv.js"></script>
    <script src="${contextPath}/resources/js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-layout">
<div class="main-container">
    <div class="main-content">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">
                    <div class="center">
                        <h1>
                            <i class="icon-leaf green"></i>
                            <span class="red">Tanx</span>
                            <span class="white">Admin</span>
                        </h1>
                        <h4 class="blue">&copy; tanx</h4>
                    </div>

                    <div class="space-6"></div>

                    <div class="position-relative">
                        <div id="login-box" class="login-box visible widget-box no-border">
                            <div class="widget-body">
                                <div class="widget-main">
                                    <h4 class="header blue lighter bigger">
                                        <c:if test="${empty shiroLoginFailure}">
                                            <i class="icon-coffee green"></i>
                                            请输入您的用户名密码
                                        </c:if>

                                        <c:if test="${not empty shiroLoginFailure}">
                                            用户名或密码错误
                                            <span class="hide">${shiroLoginFailure}</span>
                                        </c:if>

                                        <c:if test="${not empty param.err}">
                                            <i class="icon-coffee green"></i>
                                            无权限
                                        </c:if>
                                    </h4>


                                    <div class="space-6"></div>

                                    <form action="${contextPath}/login.htm" method="post">
                                        <fieldset>
                                            <label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" class="form-control" name="username"
                                                                   placeholder="Username"/>
															<i class="icon-user"></i>
														</span>
                                            </label>

                                            <label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" name="password"
                                                                   placeholder="Password"/>
															<i class="icon-lock"></i>
														</span>
                                            </label>

                                            <div class="space"></div>

                                            <div class="clearfix">
                                                <%--<label class="inline">--%>
                                                <%--<input type="checkbox" class="ace"/>--%>
                                                <%--<span class="lbl"> Remember Me</span>--%>
                                                <%--</label>--%>

                                                <button type="submit"
                                                        class="width-35 pull-right btn btn-sm btn-primary">
                                                    <i class="icon-key"></i>
                                                    登录
                                                </button>
                                            </div>

                                            <div class="space-4"></div>
                                        </fieldset>
                                    </form>
                                </div>

                            </div>
                        </div>

                    </div>
                    <!-- /position-relative -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
</div>
<!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->

<script type="text/javascript">
    window.jQuery || document.write("<script src='${contextPath}/resources/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='${contextPath}/resources/js/jquery-1.10.2.min.js'>" + "<" + "/script>");
</script>
<![endif]-->

<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='${contextPath}/resources/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>

</body>
</html>
