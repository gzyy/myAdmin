<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2015/9/8
  Time: 14:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>用户</title>
</head>
<body>

<%--<form:form commandName="formDto" id="formDto">--%>
<%--<form:input path="name" id="name" placeholder="请输入用户名(不能为空)"/>--%>
<%--<form:errors path="name"/>--%>
<%--<button type="submit">提交</button>--%>
<%--</form:form>--%>

<div class="breadcrumbs" id="breadcrumbs">
    <script type="text/javascript">
        try {
            ace.settings.check('breadcrumbs', 'fixed')
        } catch (e) {
        }
    </script>

    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="/index.htm">首页</a>
        </li>
        <li class="active">用户</li>
    </ul>
</div>
<div class="page-content">
    <div class="page-header">
        <h1>
            用户
            <small>
                <i class="icon-double-angle-right"></i>
                新增/修改用户
            </small>
        </h1>
    </div>
    <!-- /.page-header -->

    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <form:form commandName="formDto" id="formDto" cssClass="form-horizontal" role="form">
                <%--<form:hidden path="uuid"/>--%>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="name"> 姓名 </label>

                    <div class="col-sm-9">
                        <form:input path="name" id="name" cssClass="col-xs-10 col-sm-5" placeholder="请输入姓名(不能为空)"/>
                        <span class="help-inline col-xs-12 col-sm-7">
                            <span class="middle text-danger"><form:errors path="name"/></span>
                        </span>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <button class="btn btn-info" type="submit">
                            确定
                        </button>

                        &nbsp; &nbsp; &nbsp;
                        <a class="btn btn-success" href="list.htm" role="button">
                            返回列表
                        </a>
                    </div>
                </div>
            </form:form>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>

</body>
</html>
