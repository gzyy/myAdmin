<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2015/10/12
  Time: 15:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>用户列表</title>
</head>
<body>
<div class="breadcrumbs" id="breadcrumbs">
    <script type="text/javascript">
        try {
            ace.settings.check('breadcrumbs', 'fixed')
        } catch (e) {
        }
    </script>

    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="/index.htm">首页</a>
        </li>
        <li class="active">用户列表</li>
    </ul>
</div>

<div class="widget-main pull-left">
    <form method="get" class="form-inline">
        <input type="hidden" name="currentPage" value="${listDto.currentPage}">
        <input type="text" name="name" value="${listDto.name}" class="form-control input-small"
               placeholder="姓名">
        <button type="submit" class="btn btn-purple btn-sm">
            搜索
            <i class="icon-search icon-on-right bigger-110"></i>
        </button>

        <a class="btn btn-success btn-sm" href="add.htm" role="button">
            <i class="glyphicon glyphicon-plus"></i>
            添加
        </a>
    </form>
</div>


<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>姓名</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${listDto.list}" var="user">
            <tr>
                <td>${user.name}</td>
                <td><a href="/user/add.htm?uuid=${user.uuid}">编辑</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<custom:page page="${listDto}"/>

</body>
</html>
