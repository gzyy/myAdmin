<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2015/10/10
  Time: 1:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta charset="utf-8"/>
    <c:set var="contextPath" value="${pageContext.request.contextPath}" scope="application"/>
    <link rel="shortcut icon" type="image/x-icon" href="${contextPath}/resources/images/favicon.ico"/>

    <meta name="viewport" content="width=device-width,user-scalable=no"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="author" content="唐旭"/>

    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${contextPath}/resources/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${contextPath}/resources/css/ace-fonts.css"/>
    <link rel="stylesheet" href="${contextPath}/resources/css/ace.min.css"/>
    <link rel="stylesheet" href="${contextPath}/resources/css/ace-rtl.min.css"/>
    <link rel="stylesheet" href="${contextPath}/resources/css/ace-skins.min.css"/>
    <script src="${contextPath}/resources/js/ace-extra.min.js"></script>

    <title>系统初始化</title>

</head>
<body>
<div class="page-header">
    <h1>系统初始化
        <small>请初始化一个系统管理员</small>
    </h1>
</div>
<div class="well">
    <form:form commandName="formDto" id="formDto" cssClass="form-horizontal">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">名字</label>

            <div class="col-sm-10">
                <form:input path="name" id="name" cssClass="form-control" placeholder="请输入名字(不能为空)"/>
                <form:errors path="name" cssClass="text-danger"/>
            </div>
        </div>
        <div class="form-group">
            <label for="username" class="col-sm-2 control-label">用户名</label>

            <div class="col-sm-10">
                <form:input path="username" id="username" cssClass="form-control" placeholder="请输入用户名(不能为空)"/>
                <form:errors path="username" cssClass="text-danger"/>
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-2 control-label">密码</label>

            <div class="col-sm-10">
                <form:password path="password" id="password" cssClass="form-control" placeholder="请输入密码(不能为空)"/>
                <form:errors path="password" cssClass="text-danger"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success">确定</button>
            </div>
        </div>
    </form:form>
</div>


</body>
</html>
