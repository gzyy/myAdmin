<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="page" required="true" type="com.tanx.dto.Page" %>

<c:set var="first" value="${page.first}"/>
<c:set var="last" value="${page.last}"/>
<%--这里主要考虑用 displayTag 还是 直接自定义--%>
<%--displayTag主要考虑的是 将table看作一个整体 分页只是一个组成部分--%>
<%--自定义则是将分页作为一个整体来对待--%>
<nav class="pull-right">
    <ul class="pagination">
        <li class="${first?"disabled":""}">
            <c:if test="${not first}">
            <a href="javascript:toPage(${page.currentPage-1})" aria-label="Previous">
                </c:if>
                <span aria-hidden="true">&laquo;</span>
                <c:if test="${not first}">
            </a>
            </c:if>
        </li>
        <c:forEach begin="${page.beginPageNumber}" end="${page.endPageNumber}" var="index">
            <li class="${index eq page.currentPage?"active":""}"><a href="javascript:toPage(${index})">${index}</a></li>
        </c:forEach>
        <li class="${last?"disabled":""}">
            <c:if test="${not last}">
            <a href="javascript:toPage(${page.currentPage+1})" aria-label="Next">
                </c:if>
                <span aria-hidden="true">&raquo;</span>
                <c:if test="${not last}">
            </a>
            </c:if>
        </li>
    </ul>
</nav>

<script type="application/javascript">
    function changeURLArg(url, arg, arg_val) {
        var pattern = arg + '=([^&]*)';
        var replaceText = arg + '=' + arg_val;
        if (url.match(pattern)) {
            var tmp = '/(' + arg + '=)([^&]*)/gi';
            tmp = url.replace(eval(tmp), replaceText);
            return tmp;
        } else {
            if (url.match('[\?]')) {
                return url + '&' + replaceText;
            } else {
                return url + '?' + replaceText;
            }
        }
        return url + '\n' + arg + '\n' + arg_val;
    }

    function toPage(pageNumber) {
        var url = this.location.href.toString();
        var arg = changeURLArg(url, "currentPage", pageNumber);
        this.location.href = arg;
    }
</script>
