package com.tanx.dao;

import com.tanx.entity.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by 唐旭 on 2015/7/8.
 */
@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String>,
        JpaSpecificationExecutor<User> {
}
