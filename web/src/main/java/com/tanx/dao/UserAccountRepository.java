package com.tanx.dao;

import com.tanx.entity.UserAccount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by 唐旭 on 2015/7/8.
 */
@Repository
public interface UserAccountRepository extends PagingAndSortingRepository<UserAccount, String>,
        JpaSpecificationExecutor<UserAccount> {
    UserAccount findByUsername(String username);

    Page<UserAccount> findByPassword(String password, Pageable pageable);
}
