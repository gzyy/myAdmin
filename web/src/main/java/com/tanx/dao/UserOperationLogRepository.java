package com.tanx.dao;

import com.tanx.entity.system.UserOperationLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by 唐旭 on 2015/10/12.
 */
public interface UserOperationLogRepository extends
        PagingAndSortingRepository<UserOperationLog, String>,
        JpaSpecificationExecutor<UserOperationLog> {
}
