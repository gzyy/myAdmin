package com.tanx.dao.builder;

import com.tanx.entity.User;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.Map;

/**
 * Created by 唐旭 on 2015/10/11.
 */
public class UserListSpecificationBuilder {
    public static Specification<User> buildUserList(final Map<String, Object> query) {
        return new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                String name = (String) query.get("name");
                if (StringUtils.isEmpty(name)) {
                    return criteriaBuilder.conjunction();
                }
                Path<String> namePath = root.get("name");
                return criteriaBuilder.like(namePath, "%" + name + "%");
            }
        };
    }
}
