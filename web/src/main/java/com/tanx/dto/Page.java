package com.tanx.dto;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Tanx on 2015/9/5.
 */
public abstract class Page<T> {

    public static int DEFAULT_PAGE_SIZE = 10;
    public static int DEFAULT_PAGE_BUTTON = 5;

    protected Map<String, Object> query = new HashMap<String, Object>();

    protected int currentPage = 1;
    protected int rowSize;
    protected long totalRecord;
    protected int totalPage;
    protected List<T> list = new ArrayList<T>();

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public int getCurrentPage() {
        if (currentPage <= 0) {
            return 1;
        }
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        if (currentPage <= 0) {
            currentPage = 1;
        }
        this.currentPage = currentPage;
    }

    public int getRowSize() {
        if (rowSize > 0) {
            return rowSize;
        }
        return DEFAULT_PAGE_SIZE;
    }

    public void setRowSize(int rowSize) {
        if (rowSize <= 0) {
            rowSize = DEFAULT_PAGE_SIZE;
        }
        this.rowSize = rowSize;
    }

    public long getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(long totalRecord) {
        this.totalRecord = totalRecord;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public abstract void addAllList(List allUsers);

    public boolean isFirst() {
        return currentPage == 1;
    }

    public boolean isLast() {
        return currentPage == totalPage;
    }

    public int getBeginPageNumber() {
        int result = currentPage - (DEFAULT_PAGE_BUTTON / 2);
        return result > 0 ? result : 1;
    }

    public int getEndPageNumber() {
        int result = currentPage + (DEFAULT_PAGE_BUTTON / 2);
        return result < totalPage ? result : totalPage;
    }

    public abstract Map<String, Object> getQuery();

    //TODO:这两个方法本来应该放在专门的转换类中
    //有专门的一层来做 dto和domain 之间的转换
    public Pageable toPageable() {
        return new PageRequest(getCurrentPage() - 1, getRowSize());
    }

    public void loadList(org.springframework.data.domain.Page page) {
        this.setTotalPage(page.getTotalPages());
        this.setTotalRecord(page.getTotalElements());
        this.addAllList(page.getContent());
    }

}
