package com.tanx.dto.init;

/**
 * Created by 唐旭 on 2015/10/10.
 */
public class InitFormDto {
    private String name;
    private String username;
    private String password;

    public String getName() {
        return name;
    }

    public InitFormDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public InitFormDto setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public InitFormDto setPassword(String password) {
        this.password = password;
        return this;
    }
}
