package com.tanx.dto.user;

import com.tanx.entity.User;

/**
 * Created by 唐旭 on 2015/10/12.
 */
public class UserFormDto {
    private String uuid;
    private String name;

    public UserFormDto() {
    }

    public UserFormDto(User user) {
        this.uuid = user.getUuid();
        this.name = user.getName();
    }

    public String getUuid() {
        return uuid;
    }

    public UserFormDto setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserFormDto setName(String name) {
        this.name = name;
        return this;
    }
}
