package com.tanx.dto.user;

import com.tanx.entity.User;

/**
 * Created by 唐旭 on 2015/10/11.
 */
public class UserDto {
    private String name;
    private String uuid;

    public UserDto(User user) {
        this.name = user.getName();
        this.uuid = user.getUuid();
    }

    public String getName() {
        return name;
    }

    public UserDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getUuid() {
        return uuid;
    }

    public UserDto setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }
}
