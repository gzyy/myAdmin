package com.tanx.dto.user;

import com.tanx.dto.Page;
import com.tanx.entity.User;

import java.util.List;
import java.util.Map;

/**
 * Created by 唐旭 on 2015/10/11.
 */
public class UserListDto extends Page<UserDto> {
    private String name;

    public String getName() {
        return name;
    }

    public UserListDto setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public void addAllList(List allUsers) {
        for (Object element : allUsers) {
            list.add(new UserDto((User) element));
        }
    }

    @Override
    public Map<String, Object> getQuery() {
        query.put("name", name);
        return query;
    }
}
