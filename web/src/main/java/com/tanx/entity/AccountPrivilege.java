package com.tanx.entity;

import com.tanx.context.BeanProvider;
import com.tanx.dao.AccountPrivilegeRepository;
import com.tanx.dao.UserRepository;
import com.tanx.utils.UuidGenerator;

import javax.persistence.*;

/**
 * Created by 唐旭 on 2015/7/8.
 */
@Entity
@Table(name = "t_AccountPrivilege")
public class AccountPrivilege {

    private transient AccountPrivilegeRepository repository = BeanProvider.getBean(AccountPrivilegeRepository.class);

    @Id
    private String uuid = UuidGenerator.generate();
    @ManyToOne
    @JoinColumn(name = "userAccount_id")
    private UserAccount userAccount;

//  资源id
//  private String resourceId;

    @Enumerated(EnumType.STRING)
    private Privilege privilege;

    public String getUuid() {
        return uuid;
    }

    public AccountPrivilege setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public AccountPrivilege setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
        return this;
    }

    public Privilege getPrivilege() {
        return privilege;
    }

    public AccountPrivilege setPrivilege(Privilege privilege) {
        this.privilege = privilege;
        return this;
    }

    public String getRoleAsString() {
        return "";
    }

    public String getPermissionAsString() {
        return privilege.getPermissionAsString();
    }

    public AccountPrivilege save() {
        repository.save(this);
        return this;
    }
}
