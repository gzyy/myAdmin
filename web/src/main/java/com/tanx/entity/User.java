package com.tanx.entity;

import com.tanx.context.BeanProvider;
import com.tanx.dao.UserRepository;
import com.tanx.utils.UuidGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 唐旭 on 2015/7/8.
 */
@Entity
@Table(name = "t_User")
public class User implements Serializable {
    private transient UserRepository userRepository = BeanProvider.getBean(UserRepository.class);

    @Id
    private String uuid = UuidGenerator.generate();

    @Column
    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private List<UserAccount> userAccountList = new ArrayList<UserAccount>();

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public User setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public List<UserAccount> getUserAccountList() {
        return userAccountList;
    }

    public User setUserAccountList(List<UserAccount> userAccountList) {
        this.userAccountList = userAccountList;
        return this;
    }

    public User save() {
        userRepository.save(this);
        return this;
    }

    public User addUserAccount(UserAccount userAccount) {
        userAccountList.add(userAccount);
        return this;
    }
}
