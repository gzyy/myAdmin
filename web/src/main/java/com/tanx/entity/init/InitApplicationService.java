package com.tanx.entity.init;

import com.tanx.entity.AccountPrivilege;
import com.tanx.entity.Privilege;
import com.tanx.entity.User;
import com.tanx.entity.UserAccount;

import java.util.ArrayList;
import java.util.List;

/**
 * 负责整个系统初始化工作(领域驱动中,领域服务对象)
 * Created by 唐旭 on 2015/10/10.
 */
public class InitApplicationService {

    public void initApplication(String name, String username, String password) {
        User user = new User().setName(name);
        UserAccount userAccount = new UserAccount().setUsername(username).setPassword(password);
        userAccount.addPrivileges(getAllAccountPrivileges());
        user.addUserAccount(userAccount);
        user.save();
    }

    private List<AccountPrivilege> getAllAccountPrivileges() {
        Privilege[] values = Privilege.values();
        List<AccountPrivilege> list = new ArrayList<AccountPrivilege>(values.length);
        for (Privilege value : values) {
            AccountPrivilege accountPrivilege = new AccountPrivilege().setPrivilege(value);
            list.add(accountPrivilege);
        }
        return list;
    }
}
