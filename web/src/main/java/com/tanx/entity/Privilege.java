package com.tanx.entity;

import org.apache.commons.lang.StringUtils;

/**
 * Created by 唐旭 on 2015/7/8.
 */
public enum Privilege {
    USER_MANAGER_ALL("用户管理", "USER_MANAGER", "*"),
    USER_MANAGER_EDIT("用户管理", "USER_MANAGER", "EDIT");

    Privilege(String label, String code) {
        this.label = label;
        this.code = code;
    }

    Privilege(String label, String code, String operate) {
        this.label = label;
        this.code = code;
        this.operate = operate;
    }

    private String label;
    private String code;
    private String operate;

    private static final String SPLIT = ":";

    public String getLabel() {
        return label;
    }

    public Privilege setLabel(String label) {
        this.label = label;
        return this;
    }

    public String getPermissionAsString() {
        if (StringUtils.isNotEmpty(operate)) {
            return code + SPLIT + operate;
        }
        return code;
    }
}
