package com.tanx.entity;

import com.tanx.context.BeanProvider;
import com.tanx.dao.UserAccountRepository;
import com.tanx.utils.UuidGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 唐旭 on 2015/7/8.
 */
@Entity
@Table(name = "t_UserAccount")
public class UserAccount {
    private transient UserAccountRepository repository = BeanProvider.getBean(UserAccountRepository.class);

    @Id
    private String uuid = UuidGenerator.generate();

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column
    private String username;
    @Column
    private String password;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "userAccount_id")
    private List<AccountPrivilege> privilegeList = new ArrayList<AccountPrivilege>();

    public String getUsername() {
        return username;
    }

    public UserAccount setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserAccount setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getUuid() {
        return uuid;
    }

    public UserAccount setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public List<AccountPrivilege> getPrivilegeList() {
        return privilegeList;
    }

    public UserAccount setPrivilegeList(List<AccountPrivilege> privilegeList) {
        this.privilegeList = privilegeList;
        return this;
    }

    public User getUser() {
        return user;
    }

    public UserAccount setUser(User user) {
        this.user = user;
        return this;
    }

    public void addPrivilege(AccountPrivilege accountPrivilege) {
        this.privilegeList.add(accountPrivilege);
    }

    public UserAccount save() {
        repository.save(this);
        return this;
    }

    public void addPrivileges(List<AccountPrivilege> allAccountPrivileges) {
        for (AccountPrivilege accountPrivilege : allAccountPrivileges) {
            addPrivilege(accountPrivilege);
        }
    }
}
