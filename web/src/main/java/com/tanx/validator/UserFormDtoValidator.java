package com.tanx.validator;

import com.tanx.dto.user.UserFormDto;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by ���� on 2015/10/12.
 */
@Component
public class UserFormDtoValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return UserFormDto.class.equals(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserFormDto formDto = (UserFormDto) target;
        validateName(formDto, errors);
    }

    private void validateName(UserFormDto formDto, Errors errors) {
        String name = formDto.getName();
        if (StringUtils.isEmpty(name)) {
            errors.rejectValue("name", null, "��������Ϊ��");
        }
    }
}
