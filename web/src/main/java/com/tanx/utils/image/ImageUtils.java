package com.tanx.utils.image;

import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by 唐旭 on 2015/7/8.
 */
public abstract class ImageUtils {
    public static final java.util.List<String> IMAGE_EXTENSIONS = Arrays.asList("png", "jpg", "jpeg", "gif", "bmp");
    public static final String DEFAULT_IMAGE_TYPE = "jpg";

    //default
    protected ImageUtils() {
    }

    /**
     * Check the image name is image image or not.
     *
     * @param filename File name
     * @return True is image, otherwise false
     */
    public static boolean isImageFilename(String filename) {
        if (StringUtils.isEmpty(filename)) {
            return false;
        }
        String extension = FilenameUtils.getExtension(filename);
        return IMAGE_EXTENSIONS.contains(extension.toLowerCase());
    }


    /**
     * 缩放图片
     * Reduce image
     *
     * @param origImg   Original image
     * @param newWidth  reduce image width
     * @param newHeight reduce image height
     * @param keep      是否强制缩放 到指定的大小, false是,true则按照原始图片比例缩放
     * @return Reduce image as byte
     * @throws IOException IOException
     */
    public static byte[] reduce(BufferedImage origImg, int newWidth, int newHeight, boolean keep) throws IOException {
        final int origWidth = origImg.getWidth();
        final int origHeight = origImg.getHeight();
        if (newWidth > origWidth) {
            newWidth = origWidth;
        }
        if (newHeight > origHeight) {
            newHeight = origHeight;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Thumbnails.of(origImg)
                .size(newWidth, newHeight)
                .keepAspectRatio(keep)
                .outputFormat(DEFAULT_IMAGE_TYPE)
                .toOutputStream(baos);

//        final Image scaledInstance = origImg.getScaledInstance(newWidth, newHeight, Image.SCALE_DEFAULT);
//        BufferedImage newImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
//
//        final Graphics graphics = newImage.getGraphics();
//        graphics.drawImage(scaledInstance, 0, 0, null);
//        graphics.dispose();

        return baos.toByteArray();
    }

    /**
     * 裁剪图片
     * Crop image
     *
     * @param origImg   Original image data
     * @param newWidth  cut image newWidth
     * @param newHeight cut image newHeight
     * @param newX      cut image x (left top)
     * @param newY      cut image y (left top)
     * @return cut image bytes
     * @throws IOException IOException
     */
    public static byte[] crop(BufferedImage origImg, int newWidth, int newHeight, int newX, int newY) throws IOException {

        final int origWidth = origImg.getWidth();
        final int origHeight = origImg.getHeight();
        if (newX + newWidth > origWidth) {
            newWidth = origWidth;
        }
        if (newY + newHeight > origHeight) {
            newHeight = origHeight;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Thumbnails.of(origImg)
                .sourceRegion(newX, newY, newWidth, newHeight)
                .size(newWidth, newHeight)
                .keepAspectRatio(false)
                .outputFormat(DEFAULT_IMAGE_TYPE)
                .toOutputStream(baos);
        return baos.toByteArray();

//        Image image = origImg.getScaledInstance(origWidth, origHeight, Image.SCALE_DEFAULT);
//        BufferedImage newImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
//        Graphics2D g2d = newImage.createGraphics();
//        if (pngOrGif) {
//            newImage = g2d.getDeviceConfiguration().createCompatibleImage(newWidth, newHeight, Transparency.TRANSLUCENT);
//            g2d.dispose();
//            g2d = newImage.createGraphics();
//        }
//        g2d.drawImage(image, 0, 0, newWidth, newHeight, newX, newY, newX + newWidth, newY + newHeight, null);
//        g2d.dispose();
//
//        return imageAsBytes(newImage);
    }

    /**
     * 将 文件大小转化为 对应的文字显示
     * 如 1024 ->  1 K
     *
     * @param size size
     * @return size as text
     */
    public static String sizeAsText(long size) {
        if (size < 0) {
            return "NaN";
        }
        String text;
        if (size < 1024) {
            text = size + "B";
        } else if (size < 1048576) {
            text = size / 1024l + "K";
        } else if (size < 1073741824) {
            text = size / 1048576l + "M";
        } else {
            text = size / 1073741824l + "G";
        }
        return text;
    }
}
