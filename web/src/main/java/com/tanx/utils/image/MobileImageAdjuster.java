package com.tanx.utils.image;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by 唐旭 on 2015/7/8.
 */
public class MobileImageAdjuster {

    public static final int MOBILE_IMAGE_WIDTH = 480;
    private static Logger logger = LoggerFactory.getLogger(MobileImageAdjuster.class);

    private byte[] data;

    public MobileImageAdjuster(byte[] data) {
        this.data = data;
    }

    public byte[] adjust() throws IOException {
        return adjust(MOBILE_IMAGE_WIDTH);
    }

    public byte[] adjust(int width) throws IOException {
        if (width <= 0) {
            width = MOBILE_IMAGE_WIDTH;
        }

        final BufferedImage image = ImageIO.read(new ByteArrayInputStream(data));
        final int imageHeight = image.getHeight();
        final int imageWidth = image.getWidth();

        if (imageWidth > width) {
            logger.debug("The image width is {}, larger than mobile max width {}, will adjust it", imageWidth, width);
            int newHeight = newHeight(imageHeight, imageWidth, width);
            data = ImageUtils.reduce(image, width, newHeight, true);
        }

        return data;
    }

    private int newHeight(int imageHeight, int imageWidth, int width) {
        final BigDecimal newHeight = (new BigDecimal(width).multiply(new BigDecimal(imageHeight)))
                .divide(new BigDecimal(imageWidth), 2, BigDecimal.ROUND_HALF_UP);
        return newHeight.intValue();
    }
}