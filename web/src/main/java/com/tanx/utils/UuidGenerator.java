
package com.tanx.utils;

import java.util.UUID;

/**
 * Created by 唐旭 on 2015/7/8.
 */
public abstract class UuidGenerator {
    private UuidGenerator() {
    }

    public static String generate() {
        return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    }
}