package com.tanx.controller;

import com.tanx.entity.UserAccount;
import com.tanx.service.UserService;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.Collection;

/**
 * Created by 唐旭 on 2015/7/8.
 */
@Controller
public class LoginController {

    @Resource(name = "sessionDAO")
    private SessionDAO sessionDAO;

    @Autowired
    private UserService userService;

    //session 列表
    @RequestMapping("/userList.htm")
    public String userList(Model model) {
        Collection<Session> userList = sessionDAO.getActiveSessions();
        model.addAttribute("userList", userList);
        return "userList";
    }

    //杀死某一个session
    @RequestMapping("/kill.htm")
    public String kill(String sessionId, Model model) {
        Session session = sessionDAO.readSession(sessionId);
        UserAccount user = (UserAccount) session.getAttribute("user");
        if (user != null) {
            model.addAttribute("kill", user.getUser().getName());
        }
        session.stop();
        return "redirect:userList.htm";
    }
}
