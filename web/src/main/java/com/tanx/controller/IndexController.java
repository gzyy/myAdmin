package com.tanx.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by 唐旭 on 2015/10/10.
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    public String root() {
        return "redirect:index.htm";
    }

    @RequestMapping("/login.htm")
    public String login() {
        return "login";
    }

    @RequestMapping("/index.htm")
    public String index() {
        return "index";
    }

    @RequestMapping("/404.htm")
    public String notFound() {
        return "error/404";
    }
}
