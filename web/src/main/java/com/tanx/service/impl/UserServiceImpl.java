package com.tanx.service.impl;

import com.tanx.annotation.Log;
import com.tanx.dao.UserRepository;
import com.tanx.dao.builder.UserListSpecificationBuilder;
import com.tanx.dto.user.UserFormDto;
import com.tanx.dto.user.UserListDto;
import com.tanx.entity.User;
import com.tanx.service.UserService;
import com.tanx.service.operator.UserFormDtoPersister;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

/**
 * Created by 唐旭 on 2015/7/8.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void loadUserListDto(UserListDto listDto) {
        Page<User> users = userRepository.findAll(UserListSpecificationBuilder.buildUserList(listDto.getQuery())
                , listDto.toPageable());
        listDto.loadList(users);
    }

    @Override
    public UserFormDto loadUserFormDto(String uuid) {
        if (StringUtils.isEmpty(uuid)) {
            return new UserFormDto();
        }
        return new UserFormDto(userRepository.findOne(uuid));
    }

    @Override
    @Log(description = "创建/更新 用户${formDto.name}(${formDto.uuid})")
    public void persistUserFormDto(UserFormDto formDto) {
        UserFormDtoPersister persister = new UserFormDtoPersister(formDto);
        persister.persist();
    }
}
