package com.tanx.service.impl;

import com.tanx.annotation.Log;
import com.tanx.dao.UserRepository;
import com.tanx.dto.init.InitFormDto;
import com.tanx.entity.User;
import com.tanx.entity.init.InitApplicationService;
import com.tanx.service.InitAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 * 初始化系统
 * Created by 唐旭 on 2015/10/10.
 */
@Service
public class InitAdminServiceImpl implements InitAdminService {
    @Autowired
    private UserRepository repository;

    @Override
    @Log(description = "check init user...")
    public boolean checkNotUsers() {
        Page<User> users = repository.findAll(new PageRequest(0, 1));
        return users.getContent().isEmpty();
    }

    @Override
    @Log(description = "初始化第一个用户 名称:${formDto.name},用户名:${formDto.username}")
    public void initApplication(InitFormDto formDto) {
        InitApplicationService service = new InitApplicationService();
        service.initApplication(formDto.getName(), formDto.getUsername()
                , formDto.getPassword());
    }
}
