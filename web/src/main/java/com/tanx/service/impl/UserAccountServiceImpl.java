package com.tanx.service.impl;

import com.tanx.annotation.Log;
import com.tanx.dao.UserAccountRepository;
import com.tanx.entity.UserAccount;
import com.tanx.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ���� on 2015/7/8.
 */
@Service(value = "userAccountService")
public class UserAccountServiceImpl implements UserAccountService {
    @Autowired
    private UserAccountRepository repository;


    @Override
    public UserAccount getUserAccountByUserName(String username) {
        return repository.findByUsername(username);
    }

    @Override
    @Log(description = "${username},${password}|,${UserAccount.password},${UserAccount.uuid}")
    public UserAccount login(String username, String password) {
        return repository.findByUsername(username);
    }

}
