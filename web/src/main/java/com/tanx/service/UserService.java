package com.tanx.service;

import com.tanx.dto.user.UserFormDto;
import com.tanx.dto.user.UserListDto;

/**
 * Created by 唐旭 on 2015/7/8.
 */
public interface UserService {
    void loadUserListDto(UserListDto listDto);

    UserFormDto loadUserFormDto(String uuid);

    void persistUserFormDto(UserFormDto formDto);
}
