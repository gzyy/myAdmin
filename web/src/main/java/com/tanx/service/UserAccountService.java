package com.tanx.service;

import com.tanx.entity.UserAccount;

/**
 * Created by 唐旭 on 2015/7/8.
 */
public interface UserAccountService {
    UserAccount getUserAccountByUserName(String username);

    UserAccount login(String username, String password);

}
