package com.tanx.service;

import com.tanx.dto.init.InitFormDto;

/**
 * Created by 唐旭 on 2015/10/10.
 */
public interface InitAdminService {
    boolean checkNotUsers();

    void initApplication(InitFormDto formDto);
}
