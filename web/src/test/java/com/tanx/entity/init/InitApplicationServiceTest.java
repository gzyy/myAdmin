package com.tanx.entity.init;

import com.tanx.context.ContextTest;
import com.tanx.dao.UserAccountRepository;
import com.tanx.dao.UserRepository;
import com.tanx.entity.User;
import com.tanx.entity.UserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.testng.annotations.Test;

/**
 * Created by 唐旭 on 2015/10/10.
 */
public class InitApplicationServiceTest extends ContextTest {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserAccountRepository accountRepository;

    @Test
    public void testInitApplication() throws Exception {
        InitApplicationService service = new InitApplicationService();
        service.initApplication("test", "123", "456");

        Iterable<User> all = repository.findAll();
        Assert.isTrue(all.iterator().hasNext());

        UserAccount account = accountRepository.findByUsername("123");
        Assert.notNull(account);
        Assert.isTrue(account.getPassword().equals("456"));
    }
}