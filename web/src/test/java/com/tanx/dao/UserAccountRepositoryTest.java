package com.tanx.dao;

import com.tanx.context.ContextTest;
import com.tanx.entity.UserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.Assert;
import org.testng.annotations.Test;

import javax.persistence.criteria.*;
import java.util.List;

/**
 * Created by 唐旭 on 2015/10/9.
 */
public class UserAccountRepositoryTest extends ContextTest {

    @Autowired
    private UserAccountRepository repository;

    @Test
    public void testFindByUserName() throws Exception {
        new UserAccount().setUsername("123").save();
        new UserAccount().setUsername("456").save();

        UserAccount userAccount = repository.findByUsername("123");
        Assert.notNull(userAccount);
    }

    @Test
    public void testFindByPassword() {
        new UserAccount().setPassword("123").save();
        new UserAccount().setPassword("123").save();

        Page<UserAccount> password = repository.findByPassword("123", new PageRequest(0, 1));
        List<UserAccount> content = password.getContent();
        Assert.isTrue(content.size() == 1);
        Assert.isTrue(password.getTotalPages() == 2);
    }

    @Test
    public void testFindAll() {
        new UserAccount().setPassword("123").save();
        new UserAccount().setPassword("456").save();

        List<UserAccount> accountList = repository.findAll(new Specification<UserAccount>() {
            @Override
            public Predicate toPredicate(Root<UserAccount> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
//                root = criteriaQuery.from(UserAccount.class);
                root.join(root.getModel().getSingularAttribute("user"), JoinType.LEFT);
                Path<String> password = root.get("password");
                return criteriaBuilder.or(criteriaBuilder.equal(password, "123"), criteriaBuilder.equal(password, "456"));
            }
        });

        Assert.isTrue(accountList.size() == 2);
    }
}