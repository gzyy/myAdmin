package com.tanx.service.impl;

import com.tanx.context.ContextTest;
import com.tanx.entity.User;
import com.tanx.service.InitAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.testng.annotations.Test;

/**
 * Created by 唐旭 on 2015/10/10.
 */
public class InitAdminServiceImplTest extends ContextTest {

    @Autowired
    private InitAdminService adminService;

    @Test
    public void testCheckNotUsers() throws Exception {
        boolean checkNotUsers = adminService.checkNotUsers();
        Assert.isTrue(checkNotUsers);

        new User().setName("111").save();

        checkNotUsers = adminService.checkNotUsers();
        Assert.isTrue(!checkNotUsers);
    }
}