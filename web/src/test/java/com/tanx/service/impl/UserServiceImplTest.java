package com.tanx.service.impl;

import com.tanx.context.ContextTest;
import com.tanx.dao.UserRepository;
import com.tanx.dao.builder.UserListSpecificationBuilder;
import com.tanx.entity.User;
import com.tanx.entity.system.UserOperationLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 唐旭 on 2015/10/12.
 */
public class UserServiceImplTest extends ContextTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testLoadUserListDto() throws Exception {
        User user = new User().setName("123").save();

        Map<String, Object> query = new HashMap<String, Object>();
        query.put("name", "1");

        Page<User> users = userRepository.findAll(UserListSpecificationBuilder.buildUserList(query),
                new PageRequest(0, 10));
        List<User> content = users.getContent();
        Assert.isTrue(content.get(0).getUuid().equals(user.getUuid()));

    }
}