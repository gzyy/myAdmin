-- ###############
--    create database , if need create, cancel the comment
-- ###############
create database if not exists test default character set utf8;
use test set default character = utf8;

-- ###############
--    grant privileges  to honyee/honyee
-- ###############
-- GRANT ALL PRIVILEGES ON honyee.* TO honyee@localhost IDENTIFIED BY "honyee";


-- ###############
-- Domain:  User
-- ###############
Drop table  if exists `User`;
CREATE TABLE `User` (
  `uuid` VARCHAR(36) NOT NULL,
  `name` VARCHAR(255),
  PRIMARY KEY  (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ###############
-- Domain:  UserAccount
-- ###############
Drop table  if exists `UserAccount`;
CREATE TABLE `UserAccount` (
  `uuid` VARCHAR(36) NOT NULL,
  `userId` VARCHAR(36) NOT NULL,
  `username` varchar(255),
  `password` varchar(255),
  PRIMARY KEY  (`uuid`),
  INDEX `userId_index` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ###############
-- Domain:  AccountPrivilege
-- ###############
Drop table  if exists `AccountPrivilege`;
CREATE TABLE `AccountPrivilege` (
  `uuid` VARCHAR(36) NOT NULL,
  `userAccountId` VARCHAR(36) NOT NULL,
  `privilege` varchar(255),
  PRIMARY KEY  (`uuid`),
  INDEX `userAccountId_index` (`userAccountId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
